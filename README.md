Just another dotVim installation
============================

My very barely modified (mostly slimmed down and made worse) dotVim 
repo based on Cody Krieger's dotVim:

https://raw.github.com/codykrieger/dotvim/

Plugins & Customizations
========================

## Plugins

```
ack            # for ack-ing within a project
align          # for auto-aligning assignment statements, etc.
closetag-vim   # for auto-closing html, xml tags
ctrlp          # textmate-like fuzzy file quick-open thingy. mapped to <super>t and ctrl-p
endwise        # auto-insert end keyword in ruby
fugitive       # for working with git in vim
gist           # create github gists right from within vim!
git            # MORE GIT
indent-object  # represents code at the same indent level as an object
nerdcommenter  # awesome automagical commenting plugin, mapped to <leader>/
nerdtree       # project drawer! hide/show mapped to <leader>n
```

## Syntaxes

```
coffee-script
cucumber
haml
jade
javascript
markdown
mustache
puppet
rspec
ruby (updated)
scala
slim
stylus
textile
```

## Customizations

- Leader set to comma (,), not backslash (\\)
- Status bar on
- Ruler on (col/row display in status bar)
- Remembers last location in a given file
- Real tabs for Makefiles
- 4-space tabs for Python files
- Automagical, syntax-aware auto-indent
- \<leader\>e autocompletion to the current dir to edit a file
- \<leader\>te autocompletion to the current dir to edit a file in a new
  tab
- ctrl-up and ctrl-down to "bubble" lines up and down in normal and
  visual modes
- Enter key remapped to :nohl to turn off search highlighting when you're done
  searching
- ~/.vim/backup directory for holding .swp files
- ctrl-k for deleting lines (dd command)
- \<leader\>tn to switch to the next tab, \<leader\>tp for previous tab

Color Schemes
=============

```
molokai (default)
irblack
vividchalk
reliable
```

